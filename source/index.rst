.. ArcBlock documentation master file, created by
   sphinx-quickstar on Fri Aug 11 21:52:37 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================================
ArcBlock 我支持你!  <3  ArcBlock, I will be there for u always & forever!
====================================


**标签:** ArcBlock | ArcBlock 生态系统| ArcBlock 区块链平台| ROBERT MAO | FLAVIEN CHARLON | JEAN CHEN | JOE WALLIN | @Phoenixwall James Chang | Ding Lei | https://t.me/ArcBlock  | www.arcblock.io 



----------------------
1.介绍(Introduce):
----------------------


    ArcBlock 是一个专注开发和部署应用的区块链平台和生态系统。  ArcBlock 通过引入开放链访问协议、基石程序、分布式订阅网关和代币经济体系等技术和设计，降低区块链技术应用门槛。值得一提的是，ArcBlock 的“矿工”不仅可以提供计算资源，而且可以提供可重用的模块、新的服务甚至是可以直接部署使用的应用。官网请移步: https://www.arcblock.io 
   The purpose of this document is to provide a high-level technical overview of the ArcBlock platform. It assumes that the reader has a basic understanding of cloud computing, decentralized applications, and blockchain technology. A list of resources is provided for those who would like to develop a foundational understanding of blockchain technology. Refer to https://www.arcblock.io

     .. image:: ./_static/1.png

    特性Feature：

     .. image:: ./_static/2.png

    ArcBlock 在 Telegram：
    
     .. image:: ./_static/3.png
  


----------------------
2.我在行动(I am  in action):
----------------------

#a
    ---我在著名程序员博客ITEYE发表ArcBlock推文信息. I post ArcBlock info via famous it blog (ITEYE) in China

    --- http://ironurbane.iteye.com/blog/2406925 

    --- 截图(ScreenShot)：

    .. image:: ./_static/4.png



#b
    ---我在著名程序员博客开源中囯社区博客发表ArcBlock推文信息. I post ArcBlock info via famous it blog（OSCHINA）in China

    --- https://my.oschina.net/u/2395742/blog/1603712 

    --- 截图(ScreenShot)：

    .. image:: ./_static/5.png



#c
    ---我在著名社交网站Twitter发表ArcBlock推文信息.  I post ArcBlock info via famous  Society Media Website  (Twitter) 

    --- https://twitter.com/SteveYuanchunLi

    --- 截图(ScreenShot)：

    .. image:: ./_static/6.png

